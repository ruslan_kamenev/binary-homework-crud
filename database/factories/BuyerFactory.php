<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BuyerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->firstName(),
            'surname' => $this->faker->lastName(),
            'country' => $this->faker->country(),
            'city' => $this->faker->city(),
            'addressLine' => $this->faker->address(),
            'phone' => $this->faker->unique()->phoneNumber()
        ];
    }
}
