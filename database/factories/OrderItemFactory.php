<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product' => $this->faker->numberBetween(1, 20),
            'quantity' => $this->faker->numberBetween(1, 5),
            'price' => $this->faker->numberBetween(5, 1000),
            'order' => $this->faker->numberBetween(1, 20),
            'discount' => $this->faker->numberBetween(0, 50),
            'sum' => $this->faker->numberBetween(20, 5000)
        ];
    }
}
