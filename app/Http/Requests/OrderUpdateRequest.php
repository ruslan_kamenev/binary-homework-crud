<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'orderId' => 'int|min:0|required',
            'orderItems.productId' => 'int|min:0|required',
            'orderItems.productQty' => 'int|min:0|required',
            'orderItems.productDiscount' => 'int|min:0|max:100',
        ];
    }
}
